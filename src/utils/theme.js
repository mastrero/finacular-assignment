import { extendTheme } from '@chakra-ui/react';

const colors = {
  primary: '#000087',
  secondary: '#169de1',
  bg: '#e8e8e8',
  shade: '#f5f5f5',
  line: '#e6e6e6',
};

export const theme = extendTheme({
  colors,
  styles: {
    global: {
      'input:active, input:focus': {
        outline: 'none !important',
        boxShadow: 'none !important',
        borderColor: '#e6e6e6 !important',
      },
      'button:active, button:focus': {
        outline: 'none !important',
        boxShadow: 'none !important',
      },
      'button:hover': {
        opacity: '0.8',
      },
    },
  },
  components: {
    Button: {
      variants: {
        'nav-link': {
          bg: 'white',
          color: 'primary',
          fontWeight: '400',
          fontSize: '0.8rem',
          mr: '1rem',
          my: '0px',
          pl: '1rem',
          pr: '0',
          justifyContent: 'flex-start',
          borderRadius: '0 5px 5px 0',
          '& span': {
            mr: '0.8rem',
          },
          '&.active': {
            bg: 'primary',
            color: 'white',
          },
        },
      },
    },
    Input: {},
  },
});
