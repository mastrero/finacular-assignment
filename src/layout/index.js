import { Grid, useDisclosure, Box } from '@chakra-ui/react';
import Navigation from '../components/navigation';
import Track from '../components/track';
import Content from '../components/content';
import Topbar from '../components/topbar';
import Action from '../components/action';

export default function Layout() {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <>
      <Grid
        templateColumns={['1fr 100px', null, '180px 1fr 180px']}
        position="relative"
        templateRows={['50px 1fr 1fr', null, '50px 1fr']}
        gap="0"
        h="100vh">
        <Navigation open={isOpen} />
        <Topbar toggle={onToggle} />
        <Action />
        <Content />
        <Track />
      </Grid>
      <Box
        position="absolute"
        display={isOpen ? 'flex' : 'none'}
        top="0"
        left="0"
        bg="#a2a2a2"
        z-index="99"
        opacity="0.5"
        onClick={onToggle}
        w="100vw"
        h="100vh"></Box>
    </>
  );
}
