import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import Layout from './layout';

import { theme } from './utils/theme';

function App() {
  return (
    <ChakraProvider theme={theme}>
      <Layout />
    </ChakraProvider>
  );
}

export default App;
