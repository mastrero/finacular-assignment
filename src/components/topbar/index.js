import { Flex, IconButton, GridItem, Spacer } from '@chakra-ui/react';
import { RiSettings5Fill } from 'react-icons/ri';
import { FcMenu } from 'react-icons/fc';
import { IoMdNotifications } from 'react-icons/io';

export default function Topbar({ toggle }) {
  return (
    <GridItem gridArea={['1 / 1 / 2 / 2', null, '1 / 2 / 2 / 3']}>
      <Flex
        h="50px"
        justifyContent="space-between"
        alignItems="center"
        bg="white"
        borderColor="line"
        borderStyle="solid"
        borderRightWidth="1px"
        borderBottomWidth="1px">
        <Button icon={<FcMenu />} handler={toggle} />
        <Spacer />
        <Button icon={<RiSettings5Fill />} />
        <Button icon={<IoMdNotifications />} />
      </Flex>
    </GridItem>
  );
}

const Button = ({ icon, handler }) => (
  <IconButton
    h="100%"
    px="0.9rem !important"
    icon={icon}
    color="#bdbdbd"
    fontSize="1.2rem"
    bg="white"
    aria-label="Settings"
    _hover={{ background: 'white' }}
    _active={{ background: 'white' }}
    _focus={{ background: 'white' }}
    borderColor="line"
    borderStyle="solid"
    borderLeftWidth="1px"
    onClick={handler}
  />
);
