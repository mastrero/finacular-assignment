import {
  Box,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Link,
  Flex,
} from '@chakra-ui/react';
import UserImage from '../../images/user.jpg';

export default function User() {
  return (
    <Box position="absolute" bottom="10px" left="0" w="100%" bg="white">
      <Accordion allowToggle>
        <AccordionItem border="none">
          <AccordionButton
            justifyContent="space-between"
            py="0"
            _active={{ outline: 'none' }}
            _focus={{ boxShadow: 'none' }}
            _hover={{ background: 'white' }}>
            <Box
              as="img"
              src={UserImage}
              alt="Vandana M"
              w="40px"
              h="40px"
              borderRadius="50%"
              borderWidth="3px"
              borderStyle="solid"
              borderColor="primary"
              boxShadow="0 6px 8px 0px #b1b1b1"
            />
            <Box as="span" fontSize="0.8rem">
              Vandana M
            </Box>
            <AccordionIcon color="primary" />
          </AccordionButton>
          <AccordionPanel>
            <Flex flexDirection="column">
              <Link my="2px" ml="10px" _hover={{ textDecoration: 'none' }}>
                Profile
              </Link>
              <Link my="2px" ml="10px" _hover={{ textDecoration: 'none' }}>
                Settings
              </Link>
              <Link my="2px" ml="10px" _hover={{ textDecoration: 'none' }}>
                Logout
              </Link>
            </Flex>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </Box>
  );
}
