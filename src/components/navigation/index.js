import { GridItem, Box } from '@chakra-ui/react';
import Brand from './_brand';
import Links from './_links';
import User from './_user';

export default function Navigation({ open }) {
  return (
    <GridItem
      gridArea={[null, null, '1 / 1 / 3 / 2']}
      h="100%"
      transform={[`translateX(${open ? 0 : -180}px)`, null, 'translateX(0px)']}
      transition="transform 0.35s ease"
      zIndex="100"
      position={['absolute', null, 'relative']}
      borderRightWidth="1px"
      borderRightStyle="solid"
      borderRightColor="shade">
      <Box position="relative" bg="white" h="100%">
        <Brand />
        <Links />
        <User />
      </Box>
    </GridItem>
  );
}
