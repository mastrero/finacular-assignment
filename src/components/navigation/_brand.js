import { Box, Flex } from '@chakra-ui/react';
import Finacular from '../../images/finacular_logo.png';
// import { IconButton } from '@chakra-ui/react';
// import { IoIosArrowBack } from 'react-icons/io';

export default function BRand() {
  return (
    <Flex h="50px" py="5px" pl="0.5rem" justifyContent="space-between" alignItems="center">
      <Flex justifyContent="flex-start" alignItems="center" as="a" href="/" display="flex">
        <Box as="img" src={Finacular} alt="Finacular.in" w="45px" h="45px" />
        <Box as="span" fontSize="0.9rem">
          Finacular
        </Box>
      </Flex>
      {/* <IconButton
        display={['none', null, 'block']}
        icon={<IoIosArrowBack />}
        fontSize="1.3rem"
        bg="white"
        aria-label="Navigation Toggler"
        _active={{ bg: 'none !important' }}
        _hover={{ bg: 'none !important' }}
        _focus={{ bg: 'none !important' }}
      /> */}
    </Flex>
  );
}
