import { Flex, Button } from '@chakra-ui/react';
import { MdDashboard } from 'react-icons/md';
import { FaCalendarAlt } from 'react-icons/fa';
import { FiPieChart, FiBarChart2 } from 'react-icons/fi';

export default function Links() {
  return (
    <Flex as="nav" flexDirection="column" mt="40px">
      <Button variant="nav-link" leftIcon={<MdDashboard />} className="active">
        Dashboard
      </Button>
      <Button variant="nav-link" leftIcon={<FiPieChart />}>
        Assets
      </Button>
      <Button variant="nav-link" leftIcon={<FiBarChart2 />}>
        Tracker
      </Button>
      <Button variant="nav-link" leftIcon={<FaCalendarAlt />}>
        Planning & Advisory
      </Button>
    </Flex>
  );
}
