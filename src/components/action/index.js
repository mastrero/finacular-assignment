import { Box, GridItem, Button, Flex } from '@chakra-ui/react';
import { GiElectric } from 'react-icons/gi';

export default function Actions() {
  return (
    <GridItem gridArea={['1 / 2 / 2 / 3', null, '1 / 3 / 2 / 4']}>
      <Flex
        h="50px"
        p="5px 0"
        justifyContent="center"
        alignItems="center"
        borderBottomWidth="1px"
        borderStyle="solid"
        borderColor="line">
        <Button
          p="0 0.8rem"
          fontWeight="500"
          fontSize="0.8rem"
          h="2rem"
          lineHeight="0"
          leftIcon={<Box as={GiElectric} mr="-0.3rem" />}
          bg="primary"
          color="white"
          _hover={{ opacity: 0.8 }}>
          Actions
        </Button>
      </Flex>
    </GridItem>
  );
}
