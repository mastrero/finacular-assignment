export const data = [
  { year: 1, amount: 125 },
  { year: 2, amount: 150 },
  { year: 3, amount: 250 },
  { year: 4, amount: 350 },
  { year: 5, amount: 500 },
  { year: 6, amount: 550 },
  { year: 7, amount: 650 },
  { year: 8, amount: 750 },
  { year: 9, amount: 850 },
  { year: 10, amount: 920 },
  { year: 11, amount: 1000 },
  { year: 12, amount: 1125 },
  { year: 13, amount: 1200 },
  { year: 14, amount: 1300 },
  { year: 15, amount: 1365 },
  { year: 16, amount: 1450 },
  { year: 17, amount: 1590 },
  { year: 18, amount: 1700 },
  { year: 19, amount: 1850 },
  { year: 20, amount: 2000 },
];

export const theme = {
  background: '#ffffff',
  textColor: '#000000',
  fontSize: 12,
  axis: {
    domain: {
      line: {
        stroke: '#777777',
        strokeWidth: 0,
      },
    },
    ticks: {
      line: {
        stroke: '#e0e0e0',
        strokeWidth: 0,
      },
    },
  },
  grid: {
    line: {
      stroke: '#ebebeb',
      strokeWidth: 1,
    },
  },
};
