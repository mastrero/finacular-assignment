import { Box, Flex, Button, useDisclosure } from '@chakra-ui/react';
import { data } from './_utils';
import Graph from './_graph';

export default function Chart() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const button_styles = {
    fontSize: '0.9rem',
    h: 'inherit',
    w: '30px',
    minW: '30px',
    px: '0',
    borderRadius: '5px',
    _hover: {
      opacity: 0.9,
    },
  };
  const colorScheme = trigger => (trigger ? 'primary' : 'white');

  return (
    <Box h={['350px', null, '400px']} bg="white" borderRadius="5px" border="1px solid white" borderColor="line">
      <Flex justifyContent="flex-end" p="10px">
        <Button
          onClick={onClose}
          {...button_styles}
          bg={colorScheme(!isOpen)}
          _active={{
            bg: colorScheme(!isOpen),
          }}
          color={colorScheme(isOpen)}>
          10Y
        </Button>
        <Button
          onClick={onOpen}
          {...button_styles}
          bg={colorScheme(isOpen)}
          _active={{
            bg: colorScheme(isOpen),
          }}
          color={colorScheme(!isOpen)}>
          20Y
        </Button>
      </Flex>
      <Box h="calc(100% - 50px)">
        <Graph data={isOpen ? data : [...data.slice(0, 10)]} />
      </Box>
    </Box>
  );
}
