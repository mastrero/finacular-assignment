import { theme } from './_utils';
import { linearGradientDef } from '@nivo/core';
import { Box } from '@chakra-ui/react';
import { ResponsiveBar } from '@nivo/bar';

export default function Graph({ data }) {
  return (
    <ResponsiveBar
      data={data}
      theme={theme}
      keys={['amount']}
      indexBy="year"
      margin={{ top: 20, right: 60, bottom: 50, left: 60 }}
      padding={0.5}
      borderRadius={5}
      indexScale={{ type: 'band', round: true }}
      colors={{ scheme: 'blues' }}
      defs={[
        linearGradientDef('gradient', [
          { offset: 0, color: '#000087', opacity: 1 },
          { offset: 100, color: '#169de1' },
        ]),
      ]}
      fill={[{ match: '*', id: 'gradient' }]}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 0,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'No of years',
        legendPosition: 'middle',
        legendOffset: 32,
      }}
      axisLeft={{
        tickSize: 0,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'Asset Balance ( In Lakhs )',
        legendPosition: 'middle',
        legendOffset: -40,
      }}
      enableLabel={false}
      animate={true}
      motionStiffness={90}
      motionDamping={15}
      tooltip={({ value }) => <Box>{value}</Box>}
    />
  );
}
