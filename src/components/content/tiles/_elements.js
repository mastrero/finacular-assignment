import {
  Box,
  FormControl,
  FormLabel,
  InputGroup,
  Input,
  InputLeftElement,
  InputRightElement,
  Text,
  Flex,
  Button,
  useDisclosure,
} from '@chakra-ui/react';
import { BiRupee } from 'react-icons/bi';
import Asset_Img from '../../../images/assets.png';
import Growth_Img from '../../../images/growth.png';
import Financial_Img from '../../../images/financial.png';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

export const AssetBalance = () => (
  <Flex p="0.5rem" flexWrap="nowrap" justify="space-between" alignItems="center">
    <Box>
      <FormControl>
        <FormLabel fontWeight="400" fontSize="0.7rem" py="8px">
          Enter Amount
        </FormLabel>
        <InputGroup w="75%">
          <InputLeftElement
            pointerEvents="none"
            w="auto"
            px="0.5rem"
            children={<Box as={BiRupee} color="primary" />}
          />
          <Input type="number" color="primary" bg="#d3e5f1" />
        </InputGroup>
      </FormControl>
      <Flex fontSize="0.8rem" py="10px">
        <Box as="span" mr="1.5rem">
          Last Updated
        </Box>
        <Box as="span" color="grey">
          3 Dec
        </Box>
      </Flex>
    </Box>
    <Box>
      <Box as="img" src={Asset_Img} alt="Assets" w="60px" />
    </Box>
  </Flex>
);

export const GrowthRate = () => (
  <Flex p="0.5rem" flexWrap="nowrap" justify="space-between" alignItems="center">
    <Box>
      <FormControl>
        <FormLabel fontWeight="400" fontSize="0.7rem" py="8px">
          Enter the Growth Rate
        </FormLabel>
        <InputGroup w="100px">
          <Input
            type="number"
            borderColor="secondary"
            borderWidth="2px"
            color="secondary"
            fontWeight="500"
            bg="white"
          />
          <InputRightElement children="%" color="secondary" />
        </InputGroup>
      </FormControl>
      <Flex fontSize="0.8rem" py="10px">
        <Box as="span" mr="1.5rem">
          Inflation
        </Box>
        <Box as="span" color="grey">
          6%
        </Box>
      </Flex>
    </Box>
    <Box>
      <Box as="img" src={Growth_Img} alt="Growth Rate" w="60px" />
    </Box>
  </Flex>
);

export const FinancialIndependence = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const button_styles = {
    fontSize: '0.7rem',
    h: 'inherit',
    w: '30px',
    minW: '30px',
    px: '0',
    borderRadius: '5px',
    _hover: {
      opacity: 0.9,
    },
  };

  const colorScheme = trigger => (trigger ? 'primary' : 'white');

  return (
    <Flex p="0.5rem" flexWrap="nowrap" justify="space-between" alignItems="center">
      <Box>
        <Text fontWeight="400" fontSize="0.7rem" py="8px">
          Age
        </Text>
        <Text color="primary" fontSize="1.2rem">
          45 years
        </Text>
        <Flex fontSize="0.8rem" py="10px">
          <Box as="span" mr="1.5rem">
            Optimized
          </Box>
          <Box>
            <Button
              onClick={onClose}
              {...button_styles}
              bg={colorScheme(!isOpen)}
              _active={{
                bg: colorScheme(!isOpen),
              }}
              color={colorScheme(isOpen)}>
              Yes
            </Button>
            <Button
              onClick={onOpen}
              {...button_styles}
              bg={colorScheme(isOpen)}
              _active={{
                bg: colorScheme(isOpen),
              }}
              color={colorScheme(!isOpen)}>
              No
            </Button>
          </Box>
        </Flex>
      </Box>
      <Box>
        <Box as="img" src={Financial_Img} alt="Growth Rate" w="50px" />
      </Box>
    </Flex>
  );
};

export const AverageExpenses = () => {
  return (
    <Flex p="0.5rem" flexWrap="nowrap" justify="space-between" alignItems="center">
      <Box>
        <Text fontWeight="400" fontSize="0.7rem" py="8px">
          Amount
        </Text>
        <Text color="secondary" fontSize="1.2rem">
          ₹ 25,000
        </Text>
        <Flex fontSize="0.8rem" py="10px">
          <Box as="span" mr="1.5rem">
            3 months average
          </Box>
          <Box as="span" color="grey">
            ₹ 40,000
          </Box>
        </Flex>
      </Box>
      <Box
        as={CircularProgressbar}
        w="60px"
        value={62}
        text={`${62}%`}
        styles={buildStyles({
          pathColor: '#169de1',
          textColor: 'black',
        })}
      />
    </Flex>
  );
};

export const MajorInvestments = () => (
  <Flex p="0.5rem" flexWrap="nowrap" justify="space-between" alignItems="center">
    <Box>
      <Text fontWeight="400" fontSize="0.7rem" py="8px">
        Returns generated per month
      </Text>
      <Text color="primary" fontSize="1.2rem">
        ₹ 1,25,000
      </Text>
    </Box>
    <Box mt="20px">
      <Box as="img" src={Asset_Img} alt="Assets" w="50px" />
    </Box>
  </Flex>
);
