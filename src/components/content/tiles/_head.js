import { Flex, IconButton, Text } from '@chakra-ui/react';
import { BsThreeDots } from 'react-icons/bs';

export default function Head({ title, color }) {
  return (
    <Flex
      p="0 0.5rem"
      justifyContent="space-between"
      alignItems="center"
      h="40px"
      borderBottom="1px solid white"
      borderColor="line">
      <Text as="h2" fontSize="0.8rem" color={color}>
        {title}
      </Text>
      <IconButton
        fontSize="0.9rem"
        icon={<BsThreeDots />}
        h="1rem"
        bg="white"
        _hover={{ bg: 'white' }}
        _active={{ bg: 'white' }}
        _focus={{ bg: 'white' }}
      />
    </Flex>
  );
}
