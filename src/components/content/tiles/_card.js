import { GridItem } from '@chakra-ui/react';
import Head from './_head';

export default function Card({ isBig = false, title, color, children }) {
  return (
    <GridItem bg="white" rowSpan={isBig ? 2 : 1} borderRadius="5px" border="1px solid white" borderColor="line">
      <Head title={title} color={color} />
      {children}
    </GridItem>
  );
}
