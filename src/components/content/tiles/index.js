import { Grid } from '@chakra-ui/react';
import Card from './_card';
import { AssetBalance, GrowthRate, FinancialIndependence, AverageExpenses, MajorInvestments } from './_elements';

export default function Tiles() {
  return (
    <Grid
      h="400px"
      templateRows="repeat(2, 1fr)"
      templateColumns="repeat(auto-fill, minmax(250px, 1fr))"
      gap={4}
      mt="20px">
      <Card title="Asset Balance" color="primary" children={<AssetBalance />} />
      <Card title="Growth Rate" color="secondary" children={<GrowthRate />} />
      <Card isBig={true} title="Major Investments" color="primary" children={<MajorInvestments />} />
      <Card title="Financial Independence" color="primary" children={<FinancialIndependence />} />
      <Card title="Average Expenses" color="secondary" children={<AverageExpenses />} />
    </Grid>
  );
}
