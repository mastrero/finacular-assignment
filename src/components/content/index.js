import { Box, GridItem } from '@chakra-ui/react';
import Tiles from './tiles';
import Chart from './chart';

export default function Conetnt() {
  return (
    <GridItem gridArea={['2 / 1 / 3 / 3', null, '2 / 2 / 3 / 3']} bg="shade">
      <Box overflow="hidden" h={['calc(80vh - 50px)', null, 'calc(100vh - 50px)']}>
        <Box overflow="auto" h="inherit" p={['5px', null, '2rem']}>
          <Chart />
          <Tiles />
        </Box>
      </Box>
    </GridItem>
  );
}
