import { Box, Text, Button, Flex } from '@chakra-ui/react';
import Image1 from '../../images/fin1.png';
import Image2 from '../../images/fin2.png';

export default function Cards() {
  return (
    <Flex px="0.6rem" mt={['0', null, '50px']} flexDirection={['row', null, 'column']}>
      <Card
        imageSrc={Image1}
        imageAlt="Learn Now"
        text="Understanding the power of compounding!!"
        buttonLabel="Learn Now"
      />
      <Card
        imageSrc={Image2}
        imageAlt="Track Now"
        text="Track all your expenses on a daily basis"
        buttonLabel="Track Now"
      />
    </Flex>
  );
}
const Card = ({ imageSrc, imageAlt, text, buttonLabel }) => (
  <Box w="100%" bg="shade" borderRadius="5px" border="1px solid white" borderColor="line" p="0 8px 10px" my="10px">
    <Box as="img" h="80px" m="0 auto" src={imageSrc} alt={imageAlt} />
    <Text as="p" fontSize="0.65rem" p="0.5rem 0.7rem 2rem" textAlign="center">
      {text}
    </Text>
    <Button
      fontSize="0.7rem"
      fontWeight="400"
      w="100%"
      h="1.7rem"
      bg="primary"
      color="white"
      _hover={{ opacity: 0.75 }}>
      {buttonLabel}
    </Button>
  </Box>
);
