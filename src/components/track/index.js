import { GridItem } from '@chakra-ui/react';
import Cards from './_cards';

export default function Track() {
  return (
    <GridItem gridArea={['3 / 1 / 4 / 3', null, '2 / 3 / 3 / 4']} h={['20vh', null, '100%']} bg="white">
      <Cards />
    </GridItem>
  );
}
